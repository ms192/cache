package az.ingres.redis.service;

import az.ingres.redis.dto.BookDto;

import java.util.List;

public interface BookService {
    void create(BookDto bookDto);
    List<BookDto> findAll();

    BookDto findById(Long id);

    void update(Long id, BookDto bookDto);

    void delete(Long id);

}
