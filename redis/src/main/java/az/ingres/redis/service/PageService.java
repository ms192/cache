package az.ingres.redis.service;

import az.ingres.redis.dto.BookDto;
import az.ingres.redis.dto.PageDto;

import java.util.List;

public interface PageService {
    void create(Long bookId, PageDto pageDto);
    List<PageDto> findAll(Long bookId);

    PageDto findById(Long bookId, Long pageId);

    void update(Long bookId, Long pageId, PageDto pageDto);

    void delete(Long id);
}
