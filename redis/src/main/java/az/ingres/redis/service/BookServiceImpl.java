package az.ingres.redis.service;

import az.ingres.redis.dto.BookDto;
import az.ingres.redis.mapper.BookMapper;
import az.ingres.redis.model.Book;
import az.ingres.redis.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
//@CacheConfig(cacheNames = "bookCache")
@EnableCaching
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    @Override
    public void create(BookDto bookDto) {
        Optional<Book> byName = bookRepository.findByName(bookDto.getName());
        if (byName.isPresent()) {
            throw new RuntimeException();
        }
        Book book = bookMapper.dtoToBook(bookDto);
        bookRepository.save(book);
    }
    @Cacheable(cacheNames = "books")
    @Override
    public List<BookDto> findAll() {
        return bookRepository.findAll().stream().map(bookMapper::bookToDto).toList();
    }
    @Override
    @Cacheable(key = "#id", cacheNames = "book")
    public BookDto findById(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(RuntimeException::new);
        return bookMapper.bookToDto(book);
    }
    @Override
    public void update(Long id, BookDto bookDto) {
        Book book = bookRepository.findById(id).orElseThrow(RuntimeException::new);
        book.setName(bookDto.getName());
        book.setColor(book.getColor());
        bookRepository.save(book);
    }
    @Override
    @CacheEvict(key = "#id", cacheNames = "book")
    public void delete(Long id) {
        bookRepository.deleteById(id);
    }
}
