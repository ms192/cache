package az.ingres.redis.service;

import az.ingres.redis.dto.PageDto;
import az.ingres.redis.mapper.BookMapper;
import az.ingres.redis.mapper.PageMapper;
import az.ingres.redis.model.Book;
import az.ingres.redis.model.Page;
import az.ingres.redis.repository.BookRepository;
import az.ingres.redis.repository.PageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
@Service
@RequiredArgsConstructor
public class PageServiceImpl implements PageService {
    private final PageRepository pageRepository;
    private final PageMapper pageMapper;
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    @Override
    public void create( Long bookId, PageDto pageDto) {
        Book book = bookRepository.findById(bookId).orElseThrow(RuntimeException::new);
        Page page = pageMapper.dtoToPage(pageDto);
        page.setNumberOfPage(pageDto.getNumber());
        page.setBook(book);
        pageRepository.save(page);

    }

    @Override
    public List<PageDto> findAll( Long bookId) {
        Book book = bookRepository.findById(bookId).orElseThrow(RuntimeException::new);
        List<Page> pages = book.getPageList();
        return pageMapper.pagesToDto(pages);


    }
    @Override
    public PageDto findById(Long bookId, Long pageId) {
        Book book = bookRepository.findById(bookId).orElseThrow(RuntimeException::new);
        Page page = pageRepository.findById(pageId).orElseThrow(RuntimeException::new);
        return pageMapper.pageToDto(page);
    }

    @Override
    public void update(Long bookId, Long pageId, PageDto pageDto) {
        Book book = bookRepository.findById(bookId).orElseThrow(RuntimeException::new);
        Page page = pageRepository.findById(pageId).orElseThrow(RuntimeException::new);
        page.setNumberOfPage(pageDto.getNumber());
        book.getPageList().add(page);
        pageRepository.save(page);
    }

    @Override
    public void delete(Long pageId) {
        pageRepository.deleteById(pageId);

    }
}
