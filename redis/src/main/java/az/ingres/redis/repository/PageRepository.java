package az.ingres.redis.repository;

import az.ingres.redis.model.Page;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PageRepository extends JpaRepository<Page,Long> {
}
