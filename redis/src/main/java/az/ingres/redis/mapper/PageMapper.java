package az.ingres.redis.mapper;
import az.ingres.redis.dto.PageDto;
import az.ingres.redis.model.Page;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface PageMapper {
    Page dtoToPage(PageDto pageDto);
    PageDto pageToDto(Page page);
    List<PageDto> pagesToDto(List<Page> pages);
}
