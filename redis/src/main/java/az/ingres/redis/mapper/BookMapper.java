package az.ingres.redis.mapper;
import az.ingres.redis.dto.BookDto;
import az.ingres.redis.model.Book;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BookMapper {
    Book dtoToBook(BookDto dto);
    BookDto bookToDto(Book dto);
}
