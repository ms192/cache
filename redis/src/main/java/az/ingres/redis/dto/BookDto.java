package az.ingres.redis.dto;
//import az.ingres.redis.model.Page;
import az.ingres.redis.model.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor

public class BookDto implements Serializable {
    String name;
    String color;
    List<Page> pages;
}
