package az.ingres.redis.controller;

import az.ingres.redis.dto.PageDto;
import az.ingres.redis.service.PageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/book/page")

public class PageController {
    private final PageService pageService;
    @PostMapping("/{bookId}")
    public void create(@PathVariable Long bookId, PageDto pageDto) {
        pageService.create(bookId, pageDto);
    }
    @GetMapping("/{id}")
    public List<PageDto> all(@PathVariable Long id) {
        return pageService.findAll(id);
    }
    @GetMapping("/{bookId}/{pageId}")
    public PageDto findById(@PathVariable Long bookId, @PathVariable Long pageId) {
        return pageService.findById(bookId, pageId);
    }
    @PutMapping("/{bookId}/{pageId}")
    public void update(@PathVariable Long bookId,@PathVariable Long pageId, @RequestBody PageDto pageDto) {
        pageService.update(bookId,pageId, pageDto);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        pageService.delete(id);
    }
}
