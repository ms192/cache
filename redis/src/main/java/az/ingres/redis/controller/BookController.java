package az.ingres.redis.controller;
import az.ingres.redis.dto.BookDto;
import az.ingres.redis.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/book")
public class BookController {
    private final BookService bookService;
    @PostMapping
    public void create(@RequestBody BookDto bookDto) {
        bookService.create(bookDto);
    }

    @GetMapping
    public List<BookDto> all() {
        return bookService.findAll();
    }

    @GetMapping("/{id}")
    public BookDto findById(@PathVariable Long id) {
        return bookService.findById(id);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody BookDto bookDto) {
        bookService.update(id, bookDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        bookService.delete(id);
    }


}
